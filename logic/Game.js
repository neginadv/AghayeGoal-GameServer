/**
 * Created by guy on 4/4/18.
 */
let Model = require('../repositories/Model');
let MoveValidator = require("./MoveValidator");
let CustomError = require("./CustomError");
let Field = require("./Field");
let Timer = require('./Timer');
let conf = require("./Config");
let TransactionManager = require('../repositories/TransactionManager');
let GameRepo = require("../repositories/GameRepo");

class Game extends Model {

    //TODO complete logic - used in constructor and save
    fields(){
        return {
            "_id": "",
            "machineId": ""
        }
    }


    constructor(data) {
        super();

        if (!data) data = {};
        this._id = data._id;
        this.machineId = data.machineId;
        this.players = data.players;
        this.bet = data.bet;
        if(data.field) this.field = new Field(data.field);
        this.state = data.state;

        this.turnIndex = 0; //player index in this.players[]
        this.turn = null; //playerId

        this.notify = () => {}; //will be set. sends state changes
        this.validator = new MoveValidator();
        this.timer = new Timer();
        this.disconnectionTimer = new Timer();
        this.setActionHandlers();
        this.queuedDisconnectedUser = null; //if a second user gets disconnected while another is disconnected

        //prefetching config
        this.config = conf.get();
    }


    start() {
        if(this.field === undefined) throw new CustomError("MissingData", "Field field is missing");

        this.resetPositions([0, 0]);
        this.setState(Game.WaitingForShotState);
        this.turn = this.players[0]._id.toString();

        //configs
        this.setPlayerShootTimer();
        return true;
    }


    setActionHandlers(){
        this.actionHandlers = {
            "shoot": this.shoot.bind(this),
            "move": this.move.bind(this),
            "move_validated": this.moveValidated.bind(this),
            "user_disconnected": this.userDisconnected.bind(this),
            "user_connected": this.userConnected.bind(this)
        }
    }


    action(action, user, data){
        if(this.actionHandlers[action]){
            this.actionHandlers[action](user, data)
        }
        else{
            console.log("Unsupported action for game.", action)
        }
    }


    ////////////
    // actions
    ////////////
    shoot(user, data){
        if(this.state !== Game.WaitingForShotState || this.turn !== user._id.toString()) {
            console.log("not in the right state or not ur turn: ", this.state, this.turn);
            return false;
        }

        let shotData = Game.validateShotObject(data);
        this.setState(Game.ShotInProgressState);
        this.notify("shoot", shotData, user);

        this.timer.start(this.onValidationTimedout.bind(this), this.config.VALIDATION_TIMEOUT);
        return true;
    }


    move(user, data){
        if(this.state !== Game.ShotInProgressState || this.turn !== user._id.toString()) {
            return console.log("not in the right state or not ur turn: ", this.state, this.turn);
        }
        let state = Game.validateStateObject(data);
        if(!state) {
            return console.log("move: invalid state object.", data);
        }
        console.log("move:", state);
        this.validator.pushState(state, true);
    }


    moveValidated(notUsed, newState){

        this.timer.stop(); //stop validationTimedout timer
        this.nextTurn(newState);
    }


    userConnected(user, data){
        if(this.state === Game.NotStartedState){
            this.userConnectedBeforeGameStart();
            return;
        }

        this.disconnectionTimer.stop();

        //if another player was disconnected
        if(this.queuedDisconnectedUser){
            let disconnectedUser = this.queuedDisconnectedUser;
            this.queuedDisconnectedUser = null;
            // if the queued disconnected user is this user that just connected,
            // then do nothing. all good!
            if(disconnectedUser._id.toString() !== user._id.toString())
                this.userDisconnected(disconnectedUser);
        }
        else{
            this.timer.resume(); //resume game
            this.notify("user_connected", user.opponentId);
        }
    }


    userConnectedBeforeGameStart(){
        // GAME_START_TIMEOUT logic
        // this timer will be stopped when game starts
        if(!this.timer.isActive){
            this.timer.start( () => {

                this.tie();
            }, this.config.GAME_START_TIMEOUT)
        }
    }


    userDisconnected(user, data){

        if(this.state === Game.NotStartedState){
            //remove the user object from game players, replace it with her id
            for(let i = 0; i < 2; i++){
                if(this.players[i]._id && this.players[i]._id.toString() === user._id.toString()){
                    this.players[i] = user._id.toString();
                    break;
                }
            }
        }

        //todo what happens if both players get disconnected
        if(this.state === Game.NotStartedState || this.state === Game.OverState) return;

        this.notify("user_disconnected", user.opponentId);

        //if another user is already disconnected, just add this
        //user to the set of disconnected users,
        //it will be handled after the other user connects
        if(this.disconnectionTimer.isActive){
            this.queuedDisconnectedUser = user;
            return;
        }

        this.timer.pause();
        this.disconnectionTimer.start( () => {

            let [loser, winner] = this.getUserById(user._id.toString());
            this.over(winner, loser);

        }, this.config.DISCONNECTION_TIMEOUT)
    }



    /**
     * returns an array, the first element containing the
     * user with id userIdString, and the second element,
     * the other user.
     */
    getUserById(userIdString){
        if(this.players[0]._id.toString() === userIdString){
            return [this.players[0], this.players[1]];
        }
        return [this.players[1], this.players[0]];
    }


    /**
     * this callback gets called when at least one of the players
     * didn't send the move result.
     * Either the shooter didnt send the move result,
     * or the opponent didnt send the consistency check
     * or neither did.
     */
    onValidationTimedout(){

        let unverifiedState = this.validator.popWhateverPushed();
        if(unverifiedState === null){
            //no shot result was sent from either of the players
            //end the game
            this.over(this.players[(this.turnIndex + 1) % 2], this.players[this.turnIndex])
        }
        else{
            //one of the players sent a result state,
            //use that.
            this.nextTurn(unverifiedState);
        }
    }


    setPlayerShootTimer(){
        let player = this.players[this.turnIndex];

        this.timer.start( () => {

            this.nextTurn();

        }, player.teams[player.selectedTeamIndex].time * 1000)
        //TODO more complex countdowns will be required for some fields.
    }


    setState(newState) {

        this.state = newState;
    }


    /**
     *
     * @param newState of type {ballPosition: Vec2d, players:[scored: Boolean, pieces: [Vec2d]]}
     */
    nextTurn(newState) {

        if(this.players[0]._id.toString() === this.turn){
            this.turn =  this.players[1]._id.toString();
            this.turnIndex = 1;
        }
        else{
            this.turn = this.players[0]._id.toString();
            this.turnIndex = 0;
        }
        this.setState(Game.WaitingForShotState);
        if(newState){
            this.ballPosition = newState.ballPosition;
            this.setPlayerStates(newState);
        }

        if(!this.checkGameOver()){
            this.setPlayerShootTimer();
            this.notify("turn", newState);
        }
    }


    setPlayerStates(newState){

        for(let i = 0; i < 2; i++){
            if(newState.players[i].scored){
                this.playerStates[i].score++;
            }
            this.playerStates[i].pieces = newState.players[i].pieces;
        }
    }


    checkGameOver(){
        let winner = this.isGameOver(); // returns winner index in players array
        if(winner !== false){

            console.log("Game over, winner: ", winner);
            // we have a winner
            this.over(this.players[winner], this.players[winner + 1 % 2]);
            return true;
        }
        return false;
    }


    resetPositions(scores) {
        this.playerStates = [];
        for (let i = 0; i < 2; i++) {
            //TODO foolproof
            let formation = this.players[i].formations[this.players[i].selectedFormation].positions.slice();
            this.playerStates[i] = {
                score: scores[i],
                pieces: i === 0 ? formation : formation.map(pos => [Game.FieldWidth - pos[0], Game.FieldHeight - pos[1]])
            }
        }
        this.ballPosition = Game.FieldCenter;
    }


    /**
     * do the things that need to be done when game finishes
     */
    over(winner, loser){

        if(this.state === Game.OverState) return;

        this.setState(Game.OverState);
        this.winnerId = winner._id;
        this.save();
        winner.addCurrencyAndExperience(
            2 * this.field.bet.amount,
            this.field.bet.currency,
            this.field.experience);

        loser.addExperience(this.field.loserExperience);
        this.notify("over", {winner, loser});
    }


    tie(){
        console.log("Tie");
        if(this.state === Game.OverState) return;

        this.setState(Game.OverState);
        for(let player of this.players){
            if(player && player.addCurrency){ //if it's a user model
                player.addCurrency(this.field.bet.amount, this.field.bet.currency);
            }
            else{ // if its the id of the player (user didnt join)
                TransactionManager.addTransaction(player, this.field.bet.amount, this.field.bet.currency);
            }
        }

        this.notify("tie", {});
    }


    getOpponentOf(userId){
        if(this.players[0]._id.toString() === userId.toString()){
            return this.players[1]
        }
        return this.players[0];
    }


    save(fields){
        //TODO implement
        GameRepo

    }


    hiddenFields() {
        return ["_id", "machineId", "commandMap"];
    }
}


Game.NotStartedState = 0;
Game.WaitingForShotState = 4;
Game.ShotInProgressState = 8;
Game.PlayerDisconnectedState = 12;
Game.OverState = 16;

Game.FieldWidth = 100;
Game.FieldHeight = 40;
Game.FieldCenter = [Game.FieldWidth / 2, Game.FieldHeight / 2];

/**
 *
 * @param s sample: {ballPosition: Vec2d, players:[scored: Boolean, pieces: [Vec2d]]}
 */
Game.validateStateObject = function (s) {

    if(!(
        s !== undefined
        && Array.isArray(s.ballPosition)
        && s.ballPosition.length === 2
        // && !isNaN(s.ballPosition[0])
        // && !isNaN(s.ballPosition[1])
        && Array.isArray(s.players)
        && s.players.length === 2
        && typeof(s.players[0].scored) === "boolean"
        && typeof(s.players[1].scored) === "boolean"
        && (!s.players[0].scored || !s.players[1].scored) //they can't both score
        && Array.isArray(s.players[0].pieces)
        && Array.isArray(s.players[1].pieces)
    )) return false;

    //could also check the value of each piece position dimensions
    for(let i=0; i< s.players[0].pieces.length; i++)
        if(!Array.isArray(s.players[0].pieces[i]) || s.players[0].pieces[i].length !== 2)
            return false;

    for(let i=0; i< s.players[1].pieces.length; i++)
        if(!Array.isArray(s.players[1].pieces[i]) || s.players[1].pieces[i].length !== 2)
            return false;

    return {
        ballPosition: s.ballPosition,
        players: [
            {scored: s.players[0].scored, pieces: s.players[0].pieces},
            {scored: s.players[1].scored, pieces: s.players[1].pieces},
        ]
    };
};


Game.validateShotObject = function (o, playerPiecesCount) {

    if(!o
        || isNaN(o.force)
        || isNaN(o.angle)
        || !Number.isInteger(o.piece)
        || o.piece < 0 || o.piece >= playerPiecesCount

    ) return false;

    return {
        force: o.force,
        angle: o.angle,
        piece: o.piece
    };
};

module.exports = Game;




