/**
 * Created by guy on 4/9/18.
 *
 * A timer with pause/resume capability
 */

class Timer {

    start(callback, ms){
        console.log(`Timer start(${ms})`);

        this.stop();
        this.startTime = +new Date();
        this.callback = callback;
        this.duration = ms;
        this.isActive = true;
        this.timer = setTimeout(() => {

            this.stop();
            callback();
        }, ms);
    }


    stop(){
        console.log(`Timer stop()`);
        if(this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
            this.paused = false;
            this.remaining = 0;
            this.isActive = false;
        }
    }


    pause(){
        console.log(`Timer pause()`);
        if(this.paused) return;

        this.paused = true;
        this.remaining = Math.max(0, +new Date() - this.startTime);
        clearTimeout(this.timer);
        this.timer = null;
        return this.remaining;
    }


    resume(){

        console.log(`Timer resume()`);
        if(!this.paused) return;

        console.log("resuming timer, remaining: ", this.remaining);

        this.paused = false;
        this.start(this.callback, this.remaining);
        return this.remaining;
    }


    //Do not remove this
    get(){
        return {
            remaining: Math.max(0, this.duration - (+new Date() - this.startTime))
        }
    }
}

module.exports = Timer;