/**
 * Created by guy on 4/8/18.
 *
 * Game controller
 */

let UserRepo = require("../../repositories/UserRepo");
let GameRepo = require("../../repositories/GameRepo");
let utils = require("../../services/utils");
let app = require('../../app');
let Game = require('../Game');


/**
 * Shoot handler
 * @param data
 * @param socket
 * @param io
 * @param ack
 */
exports.shoot = function (data, socket, io, ack) {

    if(socket.game.action("shoot", socket.user, data))
        if(ack)
            ack(utils.ok())
};


/**
 * move handler, sent by the shooter after shoot simulation is over client-side
 * @param data
 * @param socket
 * @param io
 */
exports.move = function (data, socket, io) {
    //if move can be accepted in this state
    //store move somewhere
    //send move to the other player to validate

    socket.game.action("move", socket.user, data);
};


/**
 * sent by the shooter's opponent after simulation is done client-side
 * @param data
 * @param socket
 * @param io
 * @returns {*}
 */
exports.consistency_check = function(data, socket, io){

    //if its a valid action for this state
    //if consistent
    //  apply move
    //  turn
    //  send state to players
    //if not consistent
    //  (solution #1) end game, winner is the other player
    //  (solution #2) check with a server side physics service
    let game = socket.game;
    if(game.state !== Game.ShotInProgressState || game.turn !== socket.user.opponentId) {
        return console.log("not in the right state or not ur turn: ", game.state, game.turn);
    }

    let state = Game.validateStateObject(data);
    if(!state) {
        return console.log("consistency_check: invalid state object.", data);
    }

    console.log("consistency_check:", state);

    game.validator.pushState(state, false);
};
