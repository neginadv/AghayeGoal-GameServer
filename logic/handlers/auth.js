/**
 * Created by guy on 4/8/18.
 *
 *  Handles user authentication and disconnection logic
 */

let UserRepo = require("../../repositories/UserRepo");
let GameRepo = require("../../repositories/GameRepo");
let utils = require("../../services/utils");
let app = require('../../app');
let eventEncoder = require('../../services/EventEncoder');
let {adManager} = require('../../services/AdManager');
let Game = require("../Game");


exports.authenticate = function ({token, gt}, socket, io, ack, onProcessed) {
    console.log("please authenticate with", token);
    if (socket.user) return;

    let gameId = gt;

    UserRepo.findByToken(token).then(user => {

        GameRepo.find(gameId, true).then( (game) => {

            if (game.machineId != app.get("uid")) {
                //TODO blacklist
                return console.warn("Attempt to connect to unassociated machine.", "game server: " + game.machineId + ", this: " + app.get("uid"));
            }

            let playerIndex = playerCanJoinGame(game);
            if(playerIndex === false) return;

            socket.user = user;
            socket.user.opponentId = game.players[(playerIndex + 1) % 2]._id ? game.players[(playerIndex + 1) % 2]._id.toString() : game.players[(playerIndex + 1) % 2];
            console.log(`User(${user._id.toString()}).op = ${user.opponentId}`);
            socket.join("u/" + user._id);
            socket.join("g/" + game._id, () => {

                socket.game = joinGame(game, user, onJoined, onGameStarted);
                onProcessed();
            });
        })
        .catch(err => {
            //TODO blacklist ? the error might be caused by sth else
            console.warn("The game was not found, id: ", gameId, err);
        });


        function playerCanJoinGame(game){

            if(game.state === Game.OverState) return false;

            /**
             * at first, game.players contains ids of players
             * which is replaced by player objects after their first connection
             */
            let playerIndex = game.players.indexOf(user._id.toString());
            if (playerIndex === -1) {
                for(let i = 0; i < 2; i++) {
                    if (game.players[i]._id && game.players[i]._id.toString() === user._id.toString()){
                        playerIndex = i;
                        break;
                    }
                }

                if(playerIndex === -1){
                    //TODO blacklist
                    console.warn("Attempt to connect to unassociated game", game);
                    return false;
                }
            }
            return playerIndex;
        }


        /**
         * onJoined is called before onGameStarted,
         */
        function onJoined(game) {
            encodeEvents(game);
            /**
             * this ack should be nowledged! before sending the game_started event
             * (or any other 4 that matter),
             * because it contains event encoding info
             */
            if (ack) ack(utils.ok({
                m: utils.encryptObject(socket.commandMap),
                game: game.get()
            }));

            game.notify = createGameEventCallback(socket, game);
            game.action("user_connected", user);
        }


        function onGameStarted(game) {
            game.validator.setCallback(createValidationCallback(socket, game));
            socket.toGame(gameId, "game_started", {game: game.get()});
            adManager.getAdsForGame(game).then(ads => {
                game.ads = ads;
                socket.toGame(game._id.toString(), "ads", ads);
            })
        }
    })
    .catch(err => {
        console.log("authentication failed.", err);
        socket.emit('authenticate', utils.nok());
    });

    // creates a commandMap for this game encoding the event names
    function encodeEvents(game) {
        if (!game.commandMap) {
            game.commandMap = eventEncoder.encodedEventNames();
        }
        socket.commandMap = game.commandMap
    }
};


function joinGame(game, user, onJoined, notifyGameStart) {
    let gameId = game._id.toString();

    if (!GameRepo.games[gameId]) {
        GameRepo.games[gameId] = game;
    }
    else {
        game = GameRepo.games[gameId]; //if game was initialized by the other p, grab that game object
    }

    if (game.players[0] == user._id.toString()) //replace player ids in game obj with user objects
        game.players[0] = user;
    else if (game.players[1] == user._id.toString())
        game.players[1] = user;
    else{ //user joined before, reconnecting or sth
        onJoined(game);
        return game;
    }

    let lastConnectingPlayer = game.players[0]._id && game.players[1]._id;
    onJoined(game, lastConnectingPlayer);

    try {
        if (lastConnectingPlayer) { //the other user connected before
            game.start();
            notifyGameStart(game)
        }
    }
    catch(e){
        console.error(e);
        //TODO do something about the CustomErrors that game.start throws
    }

    return game;
}


function createValidationCallback(socket, game) {
    return function (isConsistent, newState) {

        console.log("validation result: ", isConsistent);
        if (isConsistent) {
            game.action("move_validated", null, newState);
        }
        else {
            //TODO do something!
        }
    }
}


/**
 *
 * Game dispatches events, instead of being coupled with the
 * communication service (socket).
 * This callback here catches those events and sends the
 * appropriate events to the clients
 */
function createGameEventCallback(socket, game) {
    return  (event, data, user) => {
        console.log("game event: ", event);

        //TODO change to lookup table
        if(event === "shoot"){
            socket.toUser(user.opponentId, "opponent_shot", data);
        }
        else if(event === "turn"){
            // data can be null, meaning positions haven't changed

            socket.toGame(game._id.toString(), "turn", {turn: game.turn, playerStates: game.playerStates})
        }
        else if(event === "over"){
            socket.toUser(data.winner._id, "game_over", {won: true, user: data.winner.get()});
            socket.toUser(data.loser._id, "game_over", {won: false, user: data.loser.get()});

            GameRepo.uncache(game._id.toString());
        }
        else if(event === "tie"){
            console.log("notified: ", game._id.toString());
            socket.toGame(game._id.toString(), "tie", {});
        }
        else if(event === "user_disconnected"){
            socket.toUser(data, "user_disconnected");
        }
        else if(event === "user_connected"){
            socket.toUser(data, "user_connected");
        }
    }
}


exports.disconnect = function (data, socket) {
    console.log('user disconnected');

    socket.game.action("user_disconnected", socket.user);
};
