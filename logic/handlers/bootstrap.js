/**
 * Created by guy on 4/8/18.
 *
 * This file implements the logic of bootstrapping socket.io
 * and binding event handlers
 */
let {authenticate, disconnect} = require("./auth");
let {move, shoot, consistency_check} = require("./game");


module.exports = (io) => {
    io.on('connection', function (socket) {

        setHelperMethodsOn(socket, io);

        /**
         * Place events that you don't want encoded here
         */
        route("authenticate", authenticate, true, onAuthenticated);
        route('disconnect', disconnect);

        /**
         * after authentication, event name mappings are saved in the socket object.
         * place events that you want to be encoded here.
         */
        function onAuthenticated(){
            if(socket.installedListeners) return;
            socket.installedListeners = true;

            route("shoot", shoot);
            route("move", move);
            route("consistency_check", consistency_check);
        }


        /**
         * Sets a 'handler' for the 'event' and if !'unsecure'
         * use hashed text of 'event' as the event name.
         *
         * on processed is a custom call back function that's executed
         * when the logic of the
         * handler is executed completely
         */
        function route(event, handler, unsecure, onProcessed){

            let encodedEvent = socket.commandMap[event]|| event;
            socket.on(encodedEvent, (data, cb) => {
                if(!unsecure && !socket.user) return;
                return handler(data, socket, io, cb, onProcessed);
            })
        }
        console.log('a connection was established');
    });
};


/**
 *
 * add a bunch of helper methods to the socket object
 */
function setHelperMethodsOn(socket, io){

    /**
     * helper function, encode messages before broadcasting.
     * broadcasts to all, including the sender
     */
    socket.toUser = function(userId, event, data) {

        let encodedEvent = socket.commandMap[event] || event;
        console.log("toUser: ", userId, event, encodedEvent);
        io.to("u/" + userId).emit(encodedEvent, data);
    };


    /**
     * helper function, encode messages before broadcasting.
     * broadcasts to all, including the sender
     */
    socket.toGame = function(id, event, data) {
        let encodedEvent = socket.commandMap[event] || event;
        console.log("toGame: ", id, event, encodedEvent);
        io.to("g/" + id).emit(encodedEvent, data);
    };

    /**
     * encoded event names for this game
     */
    socket.commandMap = {};
}

