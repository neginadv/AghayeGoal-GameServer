/**
 * Created by guy on 4/17/18.
 *
 * User model
 */
let Model = require('../repositories/Model');
let TransactionManager = require('../repositories/TransactionManager');
let UserRepo = require("../repositories/UserRepo");

class User extends Model {

    constructor(data) {
        if (!data) data = {};

        super(data);

        this._id = data._id;
        this.experience = data.experience || 0;
        this.coins = data.coins || 0;
        this.level = data.level || 1;
        this.nickname = data.nickname;
        this.isGuest = data.isGuest === undefined ? true : data.isGuest;
        if (data.token) this.token = data.token;

        //TODO Validate these
        this.formations = data.formations;
        this.selectedFormation = data.selectedFormation;
        this.teams = data.teams || [];
        this.selectedTeamIndex = data.selectedTeamIndex || 0;
    }


    getCoins() {
        return this.coins;
    }


    addCurrency(amount, currency, dontSave) {

        if(isNaN(amount)) return false;

        if(currency === "coin") this.coins += amount;
        if(!dontSave) TransactionManager.addTransaction(this._id, amount, currency);
        return true;
    }


    /**
     * if possible, use this to reduce write operations to db
     */
    addCurrencyAndExperience(amount, currency, xp){
        amount = parseInt(amount);
        if(!this.addExperience(xp, true) || !this.addCurrency(amount, currency, true)) return;
        UserRepo.addCurrencyAndExperience(this._id, amount, currency, xp, this.level);
    }


    addExperience(xp, dontSave){
        console.log("addExperience: ", xp);

        xp = parseInt(xp);
        if(isNaN(xp) || xp < 0) return false;

        xp = Math.min(User.MaxExperience - this.experience, xp); //There is a max xp
        this.experience += xp;
        this.updateLevelBasedOnExperience();
        if(!dontSave) UserRepo.addExperience(this._id, xp, this.level);
        return true;
    }


    updateLevelBasedOnExperience(){

        let ceilingExperience = User.ExperienceTable[this.level];
        while(this.experience >= ceilingExperience && this.level < User.MaxLevel){
            //emit leveled_up
            this.level++;
            ceilingExperience = User.ExperienceTable[this.level];
        }
    }


    hiddenFields() {
        return ["password", "_id", "token"];
    }
}


module.exports = User;


User.MaxLevel = 150;
User.ExperienceTable = []; //defines the ceiling xp for each level
for(let i=0; i<=User.MaxLevel; i++)
    User.ExperienceTable.push(i * 100)
User.MaxExperience = User.ExperienceTable[User.MaxLevel];


