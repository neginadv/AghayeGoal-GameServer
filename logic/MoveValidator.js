/**
 * Created by guy on 4/9/18.
 *
 * This module validates client-side simulations,
 * it works like a stack,
 * wait's until both players simulate the move, then
 * checks if they both produce the same result
 */

module.exports = class MoveValidator {

    setCallback(cb) {

        this.cb = cb;
    }


    constructor() {
        this.unvalidatedStates = {};
    }


    popWhateverPushed(){
        for(let i = 0; i < 2; i ++){
            if(this.unvalidatedStates[i]){
                let result = this.unvalidatedStates[i];
                this.unvalidatedStates = {};
                return result;
            }
        }
        return null;
    }

    //
    //stores the shot result from a player, if both players call this then validation starts
    //
    pushState(move, isHisTurn) {
        console.log("push state: ", isHisTurn);

        let playerIndex = isHisTurn ? 0 : 1;
        this.unvalidatedStates[playerIndex] = move;

        if (this.unvalidatedStates[(playerIndex + 1) % 2]) {
            if (this.cb) this.cb(this.validate(), this.unvalidatedStates[0]);
            this.unvalidatedStates = {};
        }
    }


    validate() {

        console.log("checking consistency: ", this.unvalidatedStates);

        let s1 = this.unvalidatedStates[0];
        let s2 = this.unvalidatedStates[1];
        if (
            s1.players[0].score !== s2.players[0].score
            || s1.players[1].score !== s2.players[1].score
            || notCloseEnough(s1.ballPosition, s2.ballPosition)
            || s1.players[0].pieces.length !== s2.players[0].pieces.length
            || s1.players[1].pieces.length !== s2.players[1].pieces.length
        ) return false;

        for(let i=0; i<s1.players[0].pieces.length; i++)
            if(notCloseEnough(s1.players[0].pieces[i], s2.players[0].pieces[i])) return false;

        for(let i=0; i<s1.players[1].pieces.length; i++)
            if(notCloseEnough(s1.players[1].pieces[i], s2.players[1].pieces[i])) return false;

        return true;
    }
};

function notCloseEnough(v1, v2) {
    return Math.abs(v1[0] - v2[0]) + Math.abs(v1[1] - v2[1]) > 0.1;
}