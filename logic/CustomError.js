/**
 * Created by guy on 4/11/18.
 */

class CustomError {

    constructor(name, message){

        this.name = name;
        this.message = message;
    }
}