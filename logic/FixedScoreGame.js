/**
 * Created by guy on 4/4/18.
 */
let Model = require('../repositories/Model');
let MoveValidator = require("./MoveValidator");
let CustomError = require("./CustomError");
let Game = require("./Game");

module.exports = class FixedScoreGame extends Game {

    constructor(data){

        super(data);
        this.winningScore = this.field.meta.winningScore;
    }


    isGameOver(){
        for(let i = 0; i < 2; i++)
            if(this.winningScore <= this.playerStates[i].score){
                return i;
            }

        return false;
    }
};


