/**
 * Created by guy on 4/17/18.
 */





let configs = {
    "GAME_START_TIMEOUT": 12000,  // ms - time for both players to join the game
    "VALIDATION_TIMEOUT": 12000,  // ms - how much time each player has to send the shot result
    "DISCONNECTION_TIMEOUT": 10000 // ms - if the user doesnt reconnect after this much time from being disconnected, she forfeits
};

exports.get = function (key) {
    if(!key){
        return configs;
    }
    return configs[key];
};







