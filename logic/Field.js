/**
 * Created by guy on 4/17/18.
 */
let Model = require('../repositories/Model');

module.exports = class Field extends Model {

    constructor(data) {
        if (!data) data = {};

        super(data);

        this._id = data._id;
        this.name = data.name;
        this.experience = data.experience || 0;
        this.loserExperience = data.loserExperience || 0;
        this.theme = data.theme;
        this.meta = data.meta || {};
        if(data.bet) this.bet = data.bet;
    }




    hiddenFields() {
        return [];
    }
};
