/**
 * Created by guy on 4/4/18.
 */
let FixedScoreGame = require("./FixedScoreGame");


module.exports = (gameData) => {
    if(gameData.field.meta.gameType === "FixedScore")
        return new FixedScoreGame(gameData);

    //TODO add more game types
};
