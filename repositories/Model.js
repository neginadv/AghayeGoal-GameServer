/**
 * Created by guy on 4/7/18.
 */

module.exports = class Model {



    get(include) {
        let r = {};// Object.assign({}, this);

        //iteratively call get on fields (supports arrays)
        for (let key in this) {
            if (this.hasOwnProperty(key)) {
                if (Array.isArray(this[key])) {
                    r[key] = this[key].map(item => item && item.get ? item.get() : item);
                }
                else {
                    r[key] = this[key] && this[key].get ? this[key].get() : this[key];
                }
            }
        }


        if(include){
            for (let h of  this.hiddenFields())
                if(include.indexOf(h) === -1) delete r[h];
        }
        else{
            for (let h of  this.hiddenFields())
                delete r[h];
        }

        return r;
    }

    hiddenFields() {
        return [];
    }
};
