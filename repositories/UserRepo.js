let mongo = require("../services/mongo");
let collection = mongo.collection;
let utils = require("../services/utils");
let {ObjectId} = require('mongodb');
let User = require('../logic/User');
let TransactionManager = require('./TransactionManager');


exports.addExperience = function (userId, xp, level) {

    return new Promise((resolve, reject) => {
        if(xp < 1) return resolve();

        console.log("User repo.addXP: ", xp, level);
        collection('users').updateOne({"_id": userId}, {$inc: {level: level, experience: xp}}).then(() => {
            resolve();
        })
        .catch(err => {
            console.log("UserRepo.addExperience error: ", err);
            reject(err);
        });
    });
};


exports.addCurrencyAndExperience = function (userId, amount, currency, xp, level) {

    return new Promise((resolve, reject) => {
        let modifier = {level};
        if(currency === "coin") modifier.coins = amount;
        if(xp > 0) modifier.experience = xp;

        collection('users').updateOne(
            {"_id": userId},
            {$inc: modifier}
        ).then(() => {
            resolve();
        })
        .catch(err => {
            console.log("UserRepo.addCurrencyAndExperience error: ", err);
            reject(err);
        });
    });
};


exports.findByToken = function (token) {
    return new Promise((resolve, reject) => {
        collection('users').findOne({token: token}).then((user) => {
            resolve(new User(user));
        });
    })
};


exports.find = function (id) {
    return new Promise((resolve, reject) => {
        console.log("repo id: ", id);
        collection('users').findOne({"_id": ObjectId(id)}).then((user) => {
            resolve(new User(user));
        })
            .catch(err => {
                console.log("UserRepo.find error: ", err);
                reject(err);
            });
    })
};



