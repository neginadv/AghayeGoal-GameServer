let mongo = require("../services/mongo");
let collection = mongo.collection;
let utils = require("../services/utils");
let {ObjectId} = require('mongodb');
let Model = require('./Model');


TransactionManager = {

    /**
     *
     * @param userId
     * @param amount Integer, positive or negative
     * @param currency String, "coin", "gem"
     * @returns {Promise}
     */
    addTransaction: function (userId, amount, currency) {
        console.log("addTransaction: ", amount, currency);

        return new Promise((resolve, reject) => {

            amount = parseInt(amount);
            if(!amount || !currency){
                return reject();
            }

            collection("users").findOneAndUpdate(
                {_id: new ObjectId(userId.toString())},
                {$inc: {coins: amount}},
                {returnOriginal : false }

            ).then((result) => {

                //console.log("add transaction result: ", result.value);
                return resolve({coins: result.value.coins});
            })
            .catch((err) => {
                reject();
                console.log(err)});
        })
    }
};

module.exports = TransactionManager;


