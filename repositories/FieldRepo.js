/**
 * Created by guy on 4/7/18.
 */

let mongo = require("../services/mongo");
let collection = mongo.collection;
let utils = require("../services/utils");
let {ObjectId} = require('mongodb');
let Field = require('../logic/Field');


FieldRepo = {

    fields: {},


    init: function () {
        this.loadFields();
    },


    loadFields: function(){
        console.log("loading fields...");
        let self = this;
        return new Promise((resolve, reject) => {

            collection('fields').find().toArray().then(fields => {
                console.log("loaded fields: ", fields.length);
                let newFields = {};
                for(let f of fields){
                    newFields[f._id] = new Field(f);
                }
                self.fields = newFields;
                resolve();
            })
            .catch(err => {
                console.error("Field repo load error: ", err);
                reject(err);
            });
        })
    },


    isValidFieldId: function(fieldId){
        return fields[fieldId] !== undefined;
    },

};

module.exports = FieldRepo;


