let mongo = require("../services/mongo");
let collection = mongo.collection;
let utils = require("../services/utils");
let {ObjectId} = require('mongodb');
let Ad = require("../logic/Ad");


exports.findAll = function () {
    return new Promise((resolve, reject) => {

        collection('ads').find({left: {$gt: 0}}).toArray().then(items => {
            console.log("loaded ads: ", items ? items.length : 0);

            let ads = [];
            for(let item of items){
                ads.push(new Ad(item));
            }
            resolve(ads);
        })
        .catch(err => {
            console.error("ads repo load error: ", err);
            reject(err);
        });

    })
};






