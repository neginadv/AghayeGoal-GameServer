let mongo = require("../services/mongo");
let collection = mongo.collection;
let utils = require("../services/utils");
let {ObjectId} = require('mongodb');
let Game = require("../logic/Game");
let generateGame = require('../logic/GameFactory');


exports.games = {}; //caches games


exports.find = function (id, useCache) {
    return new Promise((resolve, reject) => {
        if (useCache && exports.games[id]) {
            return resolve(exports.games[id])
        }
        collection('games').findOne({"_id": ObjectId(id)}).then(game => {
            if(game) return resolve(generateGame(game));
            reject();
        }).catch(err => {
            console.warn("GameRepo.find error : ", err);
            reject(err);
        });
    })
};


/**
 *
 * @param id String
 */
exports.uncache = function (id) {
    delete exports.games[id]
};


exports.save = function (model, fields) {
    if (fields) {
        let trimmedObject = {};
        for (let key in model) {
            if (model.hasOwnProperty(key)) {
                trimmedObject[key] = model[key];
            }
        }
        if (model._id) trimmedObject._id = model._id;
        model = trimmedObject;
    }

    //TODO allow upsert
    collection("games").updateOne({"_id": model._id}, model);
};





