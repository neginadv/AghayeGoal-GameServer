/**
 * Created by guy on 3/27/18.
 */


let mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017';
const dbName = 'test';


let db = null;

exports.initialize = () => {


    return new Promise((resolve, reject) => {
        MongoClient.connect(url).then((client) => {
            console.log("Connected successfully to Mongo server");

            db = client.db(dbName);
            resolve(db);
            exports.db = db;
            exports.client = client;

            // client.close();
        })
        .catch((err) => {
            console.error("Could not connect to Mongo server");
        });
    });
};

exports.db = () => db;
exports.collection = (name) => {
    return db.collection(name);
};
exports.ObjectId = mongo.ObjectId;