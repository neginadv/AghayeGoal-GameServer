/**
 * Created by guy on 4/4/18.
 *
 * This file is supposed to be an interface to game server manager,
 * which tracks active gameServer nodes and selects one for a match
 */



let ServerManager = {
    initialize: function(){

        return new Promise((resolve, reject) => {
            resolve(true);
        });
    },


    //returns a server to be used as a game server
    register: function(){
        return new Promise((resolve, reject) => {
            resolve();
        })
    }
};

module.exports = ServerManager;

