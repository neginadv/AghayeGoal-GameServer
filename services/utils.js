
// var request = require('request');
// var config = require('./config/app');

exports.ok = function(result){
    if(result === undefined) result = {};
    result.r = 200;
    return result;
};
exports.nok = function(result){
    var data = {};
    if(Number.isInteger(result)){
        data.r = parseInt(result);
    }
    else {
        if (result === undefined) result = {};
        data = result;
        data.r = 400;
    }
    return data;
};

exports.generateToken = function(){
    return Math.random().toString(36).substr(2)
        + Math.random().toString(36).substr(2) + exports.time();
};


exports.ymd = function(){
    return (new Date()).toISOString().slice(0,10).replace(/-/g,"");
};
exports.his = function(){
    var date = new Date();
    var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds() ;
    var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes() ;
    var hour = date.getHours() < 10 ? "0" +date.getHours() : date.getHours();

    return hour + ":" + minutes + ":" +  seconds
};

exports.time = function(){
    return Math.round((+new Date()) / 1000);
};



//TODO clean!
// use the ??? library instead (cant remember the name! but it's imported in some file)
exports.xss = function(str){
    return str;
};


exports.encryptObject = function(object){

	return JSON.stringify(object);
};


// exports.authorize = function(req, res, next){
//
// 	res.ok = (data)=> res.json(exports.ok(data));
// 	res.nok = (data)=> res.json(exports.nok(data));
//     next();
// };




