/**
 * Created by guy on 3/27/18.
 *
 * loads and inits services
 */

let services = [require("./mongo"), require("./AdManager")];

module.exports = () => {
    //return new Promise((resolve, reject)=> {

    let promises = [];
    for (let service of services) {
        promises.push(service.initialize)
    }

    //serialize promise resolution
    let result = Promise.resolve();
    promises.forEach(task => {
        result = result.then(() => task());
    });
    return result;
};