/**
 * Created by guy on 4/11/18.
 *
 * encode event names that are sent/received
 */


module.exports = {


    encodedEventNames: function () {

        let commands = [
            "shoot", "move", "consistency_check",
            "game_started", "oo_match", "opponent_shot", "turn",
            "game_over", "tie"
        ];

        let codes = this.createRandomStrings(commands.length);
        let map = {};
        for (let i = 0; i < commands.length; i++) {
            map[commands[i]] = codes[i]
        }

        console.log("commandMap: ", map);
        return map;
    },


    createRandomStrings: function (n) {
        let wasGeneratedBefore = {};
        let result = [];

        for (let i = 0; i < n; i++) {
            let randomString = Math.random().toString(36).substr(2);
            while (wasGeneratedBefore[randomString])
                randomString = Math.random().toString(36).substr(2);

            result.push(randomString);
            wasGeneratedBefore[randomString] = true;
        }
        return result;
    }
};