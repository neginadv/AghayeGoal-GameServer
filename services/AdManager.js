/**
 * Created by guy on 4/4/18.
 *
 * Manages in game ads
 */

let AdRepo = require("../repositories/AdRepo");


class AdManager {

    constructor(){

        this.ads = [];
        this.nextIndex = 0;
    }


    /**
     * load ads from db
     * @returns {Promise}
     */
    initialize(){
        let self = this;
        return new Promise((resolve, reject) => {

            console.log("Loading ads...");
            AdRepo.findAll().then(ads => {
                self.ads = ads;
                resolve();
            }).catch(reject);
        })
    }


    /**
     * always resolves an array containing ad information to be shown
     * around game field
     */
    getAdsForGame(game, count){
        let self = this;
        if(!count) count = 4;
        return new Promise((resolve, reject) => {

            console.log("here0", self)
            if(self.ads.length === 0) return resolve([]);

            let ads = [];
            let skippedAds = 0;

            for(let i=0; i<count; i++){
                let ad = self.ads[self.nextIndex];
                self.nextIndex = (self.nextIndex + 1) % self.ads.length;

                if(ad.left < 0) {
                    ad.quotaFinished();
                    skippedAds++;
                    if(skippedAds >= self.ads.length) break;
                    i--;
                    continue;
                }
                ads.push(ad.media);
                ad.left--;
                ad.shown++;
            }

            resolve(ads);
        });
    }
}

let adManager = new AdManager();

exports.initialize = () => {

    return adManager.initialize();
};


exports.adManager = adManager;